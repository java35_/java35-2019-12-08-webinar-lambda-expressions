package many_args;

public class TestAppl {

	public static void main(String[] args) {
		TestAppl testAppl = new TestAppl();
		testAppl.oldSyntax();
		testAppl.newSyntax0();
		testAppl.newSyntax1();
		testAppl.newSyntax2();
	}
	
	// Java < 8
	void oldSyntax() {
		IManyArgs iManyArgs = new IManyArgs() {
			@Override
			public void method(int a, int b) {
				System.out.println(a + b);
			}
		};
		iManyArgs.method(5, 10);
	}
	
	// Java >= 8
	void newSyntax0() {
		IManyArgs iManyArgs = (a, b) -> System.out.println(a + b);
		iManyArgs.method(5, 10);
	}
	
	void newSyntax1() {
		IManyArgs iManyArgs = (int a, int b) -> System.out.println(a + b);
		iManyArgs.method(5, 10);
	}
	
	void newSyntax2() {
	}

	
}
