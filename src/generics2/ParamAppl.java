package generics2;

import java.util.Arrays;
import java.util.function.Consumer;

public class ParamAppl {
	int a = 10;
	public static void main(String[] args) {
//		Consumer<String> consumer = x -> System.out.println(x); 
//		Arrays.asList("Hello", "Hi", "Shalom")
//				.forEach(x -> System.out.println(x));
		
		ParamAppl paramAppl = new ParamAppl();
		paramAppl.boo();
	}
	
	void boo() {
		int i = 0;
		int[] array = new int[1];
		Integer in = 0;
		Arrays.asList("Hello", "Hi", "Shalom")
						.forEach(x -> array[0]++);
		
		System.out.println(array[0]);
	}
	
}
