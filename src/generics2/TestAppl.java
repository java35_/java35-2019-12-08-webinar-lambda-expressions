package generics2;

public class TestAppl {

	public static void main(String[] args) {
		TestAppl testAppl = new TestAppl();
		testAppl.oldSyntax();
		testAppl.oldSyntax1();
		testAppl.newSyntax0();
		testAppl.newSyntax1();
		testAppl.newSyntax2();
		
//		Integer a = 10;
//		Integer b = 10;
//		
//		System.out.println(a == b);
//		
//		a = 1000;
//		b = 1000;
//		System.out.println(a == b);
	}
	
	// Java < 8
	void oldSyntax() {
		IGeneric<Integer> iGeneric = new IGeneric<Integer>() {
			
			@Override
			public boolean test(Integer a, Integer b) {
//				return a.equals(b);
				int x = a;
				int y = b;
				return x == y;
			}
		};
		
		boolean bool = iGeneric.test(10, 10);
		System.out.println(bool);
	}
	
	void oldSyntax1() {
		IGeneric<Integer> iGeneric = (a, b) -> {
			int x = a;
			int y = b;
			return x == y;
		};
		boolean bool = iGeneric.test(1000, 1000);
		System.out.println(bool);
	}
	
	// Java >= 8
	void newSyntax0() {
	}
	
	void newSyntax1() {
	}
	
	void newSyntax2() {
	}

	
}
