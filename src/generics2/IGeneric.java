package generics2;

public interface IGeneric<T> {
	boolean test(T a, T b);
}
