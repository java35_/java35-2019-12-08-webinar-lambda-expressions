package with_block;

public interface IWithReturn {
	int method(int a, int b);
}
