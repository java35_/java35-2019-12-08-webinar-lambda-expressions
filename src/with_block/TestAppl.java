package with_block;

import java.util.Comparator;

public class TestAppl {

	public static void main(String[] args) {
		TestAppl testAppl = new TestAppl();
		testAppl.oldSyntax();
		testAppl.oldSyntax1();
		testAppl.newSyntax0();
		testAppl.newSyntax1();
		testAppl.newSyntax2();
		
		Comparator<Integer> comp = (o1, o2) -> o1 > o2 ? o1 : (o1.equals(o2) ? 0 : -1);
	}
	
	// Java < 8
	void oldSyntax() {
		IWithReturn iWithReturn = new IWithReturn() {
			@Override
			public int method(int a, int b) {
				int c = a + b;
				if (c > 10)
					return c;
				return -1;
			}
		};
		int j = iWithReturn.method(10, 5);
		System.out.println(j);
	}
	
	void oldSyntax1() {
		IWithReturn iWithReturn = new IWithReturn() {
			@Override
			public int method(int a, int b) {
				return a + b > 10 ? a + b : -1;
			}
		};
		int j = iWithReturn.method(10, 5);
		System.out.println(j);
	}
	
	// Java >= 8
	void newSyntax0() {
		IWithReturn iWithReturn = (a, b) -> {
			int c = a + b;
			if (c > 10)
				return c;
			return -1;
		};
		int j = iWithReturn.method(10, 5);
		System.out.println(j);
	}
	
	void newSyntax1() {
		IWithReturn iWithReturn = (a, b) -> a + b > 10 ? a + b : -1;
		int j = iWithReturn.method(10, 5);
		System.out.println(j);
	}
	
	void newSyntax2() {
	}

	
}
