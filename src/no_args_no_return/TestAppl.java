package no_args_no_return;

public class TestAppl {

	public static void main(String[] args) {
		TestAppl testAppl = new TestAppl();
		testAppl.oldSyntax();
		testAppl.newSyntax();
	}
	
	// Java < 8
	void oldSyntax() {
		INoArgsNoReturn iNoArgsNoReturn = new INoArgsNoReturn() {
			@Override
			public void method() {
				System.out.println("Hello world!");
			}
		};
		iNoArgsNoReturn.method();
	}
	
	// Java >= 8
	void newSyntax() {
		INoArgsNoReturn iNoArgsNoReturn = () -> System.out.println("Hello world!");
		iNoArgsNoReturn.method();
	}

	
}
