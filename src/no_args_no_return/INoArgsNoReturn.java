package no_args_no_return;

@FunctionalInterface
public interface INoArgsNoReturn {
	void method();
}
