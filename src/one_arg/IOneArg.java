package one_arg;

@FunctionalInterface
public interface IOneArg {
	void method(int i);
}
