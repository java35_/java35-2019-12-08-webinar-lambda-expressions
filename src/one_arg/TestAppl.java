package one_arg;

public class TestAppl {

	public static void main(String[] args) {
		TestAppl testAppl = new TestAppl();
		testAppl.oldSyntax();
		testAppl.newSyntax0();
		testAppl.newSyntax1();
		testAppl.newSyntax2();
	}
	
	// Java < 8
	void oldSyntax() {
		IOneArg iOneArg = new IOneArg() {
			@Override
			public void method(int i) {
				System.out.println(i);
			}
		};
		iOneArg.method(10);
	}
	
	// Java >= 8
	void newSyntax0() {
//		IOneArg iOneArg = (i) -> System.out.println(i);
		IOneArg iOneArg = (x) -> System.out.println(x);
		iOneArg.method(10);
	}
	
	void newSyntax1() {
		IOneArg iOneArg = i -> System.out.println(i);
		iOneArg.method(10);
	}
	
	void newSyntax2() {
		IOneArg iOneArg = (int i) -> System.out.println(i);
		iOneArg.method(10);
	}

	
}
