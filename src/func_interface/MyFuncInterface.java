package func_interface;

@FunctionalInterface
public interface MyFuncInterface {
	int method(int i);
	
	default void defaultMethod() {
		System.out.println("Default method");
	}
	
	static void staticMethod() {
		System.out.println("Static method");
	}
	
	private void privateMethod() {
		System.out.println("Private method");
	}
	
	@Override
	String toString();
}
