package with_return;

public class TestAppl {

	public static void main(String[] args) {
		TestAppl testAppl = new TestAppl();
		testAppl.oldSyntax();
		testAppl.newSyntax0();
		testAppl.newSyntax1();
		testAppl.newSyntax2();
	}
	
	// Java < 8
	void oldSyntax() {
		IWithReturn iWithReturn = new IWithReturn() {
			@Override
			public int method(int a, int b) {
				return a + b;
			}
		};
		int j = iWithReturn.method(10, 5);
		System.out.println(j);
	}
	
	// Java >= 8
	void newSyntax0() {
		IWithReturn iWithReturn = (a, b) -> a + b;
		int j = iWithReturn.method(10, 5);
		System.out.println(j);
	}
	
	void newSyntax1() {
	}
	
	void newSyntax2() {
	}

	
}
