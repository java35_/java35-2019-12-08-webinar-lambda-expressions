package with_return;

public interface IWithReturn {
	int method(int a, int b);
}
