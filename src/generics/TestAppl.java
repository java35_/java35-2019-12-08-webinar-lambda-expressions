package generics;

public class TestAppl {

	public static void main(String[] args) {
		TestAppl testAppl = new TestAppl();
		testAppl.oldSyntax();
		testAppl.oldSyntax1();
		testAppl.newSyntax0();
		testAppl.newSyntax1();
		testAppl.newSyntax2();
	}
	
	// Java < 8
	void oldSyntax() {
		IGeneric<Integer> iGeneric = new IGeneric<>() {
			@Override
			public Integer method(Integer a, Integer b) {
				return a + b;
			}
		};
		int j = iGeneric.method(5, 10);
		System.out.println(j);
	}
	
	void oldSyntax1() {
	}
	
	// Java >= 8
	void newSyntax0() {
		IGeneric<Integer> iGeneric = (a, b) -> a + b;
		int j = iGeneric.method(5, 10);
		System.out.println(j);
	}
	
	void newSyntax1() {
	}
	
	void newSyntax2() {
	}

	
}
