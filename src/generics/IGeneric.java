package generics;

public interface IGeneric<T> {
	T method(T a, T b);
}
