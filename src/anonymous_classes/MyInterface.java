package anonymous_classes;

public interface MyInterface {
	int method1(int i);
	String method2(String str);
}
