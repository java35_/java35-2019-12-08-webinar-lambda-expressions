package anonymous_classes;

public class TestAppl {
	public int a = 100;

//	public static void main(String[] args) {
//		MyInterface myInterface = new MyInterface() {
//			@Override
//			public String method2(String str) {
//				// TODO Auto-generated method stub
//				return null;
//			}
//			
//			@Override
//			public int method1(int i) {
//				// TODO Auto-generated method stub
//				return 0;
//			}
//		};
//		System.out.println(myInterface);
//		
//		Object obj = new Object();
//		System.out.println(obj);
//	}
	
//	public static void main(String[] args) {
//		MyAbstractClass myAbstractClass = new MyAbstractClass() {
//			@Override
//			int method1(int i) {
//				// TODO Auto-generated method stub
//				return 0;
//			}
//		};
//	}
	
//	public static void main(String[] args) {
//		SomeClass someClass = new SomeClass() {
//			@Override
//			int method1(int i) {
//				return i + 10;
//			}
//		};
//		SomeClass someClass2 = new SomeClass();
//		
//		System.out.println(someClass.method1(10));
//		System.out.println(someClass2.method1(10));
//	};
	
	public static void main(String[] args) {
		TestAppl testAppl = new TestAppl();
		testAppl.boo();
	}

	void boo() {
		SomeClass someClass = new SomeClass() {
			@Override
			int method1(int i) {
				return i + a;
			}
		};
		
		System.out.println(someClass.method1(10));
	}
}
